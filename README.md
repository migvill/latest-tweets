

# Tweets

Displays the last 10 tweets for any Twitter username - as long as their timeline is public.

URLs and mentions in Tweets are clickable and clicking on a Twitter screen name will load that user's last 10 tweets.

## Usage

Run with app.js

App will run locally on port 30000 and Tweets can be displayed from the url http://localhost:30000/{USERNAME}-tweets

## Developing



### Tools

Created with [Nodeclipse](https://github.com/Nodeclipse/nodeclipse-1)
 ([Eclipse Marketplace](http://marketplace.eclipse.org/content/nodeclipse), [site](http://www.nodeclipse.org))   

Nodeclipse is free open-source project that grows with your contributions.
